<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\Datos;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="app_index")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/login", name="app_login")
     */
    public function loginAction(Request $request)
    {
        return $this->render('login.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/guardar", name="app_guardar")
     */
    public function guardarAction(Request $request)
    {
        $c = curl_init('https://world-georss.waze.com/rtserver/web/TGeoRSS?tk=ccp_partner&ccp_partner_name=Municipalidad%20de%20La%20Plata&format=JSON&types=traffic,alerts,irregularities&polygon=-58.288000,-35.018000;-58.247000,-35.049000;-58.162000,-34.974000;-58.122000,-35.004000;-58.178000,-35.057000;-58.109000,-35.109000;-58.101000,-35.103000;-58.034000,-35.152000;-58.020000,-35.168000;-58.075000,-35.218000;-58.051000,-35.235000;-57.752000,-34.971000;-57.769000,-34.958000;-57.804000,-34.963000;-57.839000,-34.949000;-57.860000,-34.938000;-57.871000,-34.937000;-57.886000,-34.932000;-57.895000,-34.930000;-57.974000,-34.872000;-57.991000,-34.879000;-57.999000,-34.873000;-57.999000,-34.871000;-58.083000,-34.835000;-58.148000,-34.887000;-58.204000,-34.936000;-58.288000,-35.018000;-58.288000,-35.018000');
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

        $json = curl_exec($c);

        if (curl_error($c))
            die(curl_error($c));

        $em=$this->getDoctrine()->getManager();

        $datos = new Datos();
        $datos->setData(json_decode($json));
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $datos->setFecha(new \DateTime('NOW'));
       
        $em->persist($datos);
        $em->flush();

        return new Response($json);
    }

    /**
     * @Route("/api", name="app_api")
     */
    public function apiAction(Request $request)
    {
        $datos=$this->getDoctrine()->getRepository('AppBundle:Datos')->findBy(array(),array('id'=>'DESC'));

        //print_r(json_decode($datos[0]->getData()));
        // replace this example code with whatever you need
        return $this->json($datos[0]->getData());
    }

    /**
     * @Route("/filtrar/{fecha}", name="app_filtrar")
     */
    public function filtrarAction($fecha)
    {
        $datos=$this->getDoctrine()->getRepository('AppBundle:Datos')->findOneByFecha($fecha)->getData();

        //print_r(json_decode($datos[0]->getData()));
        // replace this example code with whatever you need
        //return new Response(json_encode($response));
        return $this->json($datos);
    }

}
